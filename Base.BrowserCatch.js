loadModernizrCheck(){

    const ua = window.navigator.userAgent;
    const msie = ua.indexOf("MSIE ");
    // 
    // console.log("User Agent: ", ua );
    // console.log("MSIE: ", msie );

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  {
        // Straight after the oFit class is applied
        setTimeout(() => {
            // grab the containers
            $('.oFit').each(function() {
                // define this
                let $container = $(this);
                // define the src
                let imgUrl = $container
                // grabs the image and utilise the src
                .find('img')
                .prop('src');
                // if it has an image
                if (imgUrl) {
                    // add a background image
                    $container
                    .css('backgroundImage', `url(${imgUrl})`)
                    .addClass('compat-object-fit');
                }
            });
        }, 600);
    }
}


export default new BrowserCatch();
